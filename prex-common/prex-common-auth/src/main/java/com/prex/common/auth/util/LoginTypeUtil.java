package com.prex.common.auth.util;

import com.prex.common.auth.service.LoginType;
import com.prex.common.core.constant.SecurityConstant;

/**
 * @Classname LoginTypeUtil
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 18:26
 * @Version 1.0
 */
public class LoginTypeUtil {

    public static LoginType getLoginType(String state) {
        if (state.equals(SecurityConstant.LOGIN_QQ)) {
            return LoginType.qq;
        } else if (state.equals(SecurityConstant.LOGIN_WEIXIN)) {
            return LoginType.weixin;
        } else if (state.equals(SecurityConstant.LOGIN_GITEE)) {
            return LoginType.gitee;
        }
        return LoginType.github;
    }
}
