package com.prex.common.message.sms.config;

import lombok.Getter;

/**
 * @Classname SmsCodeConfig
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-08-25 18:58
 * @Version 1.0
 */
@Getter
public class SmsCodeConfig {

    /**
     * 验证码长度
     */
    private int length = 6;
}
